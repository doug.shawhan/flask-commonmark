from functools import wraps
import os
from flask import Flask, render_template_string

class Fixture:
    def inject(self, auto_escape=False):
        def create_app(fn):
            @wraps(fn)
            def wrapper(*arg, **args):
                from tests.commonmark_blueprint import commonmark
                from tests.commonmark_blueprint import blueprint
                app = Flask(__name__)
                app.debug = True

                commonmark.init_app(app, auto_escape=auto_escape)
                app.register_blueprint(blueprint)

                return fn(app, *args, **args)
            return wrapper
        return create_app

fixture = Fixture()

@fixture.inject()
def test_render_inline(app):
    response = app.test_client().open("/test_inline")
    assert response.data == b"<p>Hello, <em>commonmark</em>.</p>\n"

@fixture.inject()
def test_render_var_block(app):
    response = app.test_client().open("/test_var_block")
    assert response.data == b"<p>Hello, <em>commonmark</em> block.</p>\n"

@fixture.inject()
def test_render_in_block(app):
    response = app.test_client().open("/test_in_block")
    assert response.data == b"<p>Hello, <em>commonmark</em> block.</p>\n"

@fixture.inject(auto_escape=True)
def test_render_autoescape_on_obey(app):
    response = app.test_client().open("/test_autoescape_on")
    assert response.data == b"<p>Hello, &lt;b&gt;commonmark&lt;/b&gt; block.</p>\n"

@fixture.inject(auto_escape=False)
def test_render_autoescape_off_ignore(app):
    response = app.test_client().open("/test_autoescape_off")
    print(response.data)
    assert response.data == b"<p>Hello, <b>commonmark</b> block.</p>\n"

@fixture.inject(auto_escape=False)
def test_render_autoescape_on_ignore(app):
    response = app.test_client().open("/test_autoescape_on")
    assert response.data == b"<p>Hello, <b>commonmark</b> block.</p>\n"



from functools import wraps

from flask import Flask, render_template_string

from flask_commonmark import Commonmark


class Fixture:
    def inject(self, auto_escape=False):
        def create_app(fn):
            @wraps(fn)
            def wrapper(*arg, **args):
                app = Flask(__name__)
                app.debug = True
                cm = Commonmark(app, auto_escape=auto_escape)

                @app.route("/test_inline")
                def view_render_inline():
                    mycm = "Hello, *commonmark*."
                    return render_template_string("{{mycm|commonmark}}", mycm=mycm)

                @app.route("/test_var_block")
                def view_render_var_block():
                    mycm = u"Hello, *commonmark* block."
                    template = """\
{% filter commonmark %}
{{mycm}}
{% endfilter %}"""
                    return render_template_string(template, mycm=mycm)

                @app.route("/test_in_block")
                def view_render_in_block():
                    template = u"""\
{% filter commonmark %}
Hello, *commonmark* block.
{% endfilter %}"""
                    return render_template_string(template)

                @app.route("/test_autoescape_on")
                def view_render_autoescape_on():
                    mycm = "Hello, <b>commonmark</b> block."
                    return render_template_string(
                        ("{% autoescape true %}"
                         "{{mycm|commonmark}}"
                         "{% endautoescape %}"), mycm=mycm)

                @app.route("/test_autoescape_off")
                def view_render_autoescape_off():
                    mycm = "Hello, <b>commonmark</b> block."
                    result = render_template_string(
                        ("{% autoescape false %}"
                         "{{mycm|commonmark}}"
                         "{% endautoescape %}"), mycm=mycm)
                    return result

                return fn(app, *args, **args)
            return wrapper
        return create_app

fixture = Fixture()

@fixture.inject()
def test_render_inline(app):
    response = app.test_client().open("/test_inline")
    assert response.data == b"<p>Hello, <em>commonmark</em>.</p>\n"

@fixture.inject()
def test_render_var_block(app):
    response = app.test_client().open("/test_var_block")
    assert response.data == b"<p>Hello, <em>commonmark</em> block.</p>\n"

@fixture.inject()
def test_render_in_block(app):
    response = app.test_client().open("/test_in_block")
    assert response.data == b"<p>Hello, <em>commonmark</em> block.</p>\n"

@fixture.inject(auto_escape=True)
def test_render_autoescape_on_obey(app):
    response = app.test_client().open("/test_autoescape_on")
    assert response.data == b"<p>Hello, &lt;b&gt;commonmark&lt;/b&gt; block.</p>\n"

@fixture.inject(auto_escape=False)
def test_render_autoescape_off_ignore(app):
    response = app.test_client().open("/test_autoescape_off")
    print(response.data)
    assert response.data == b"<p>Hello, <b>commonmark</b> block.</p>\n"

@fixture.inject(auto_escape=False)
def test_render_autoescape_on_ignore(app):
    response = app.test_client().open("/test_autoescape_on")
    assert response.data == b"<p>Hello, <b>commonmark</b> block.</p>\n"



.. Flask-Commonmark documentation master file, created by
   sphinx-quickstart on Tue Jun  4 08:33:53 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Flask-Commonmark
================

Add CommonMark processing filter to your `Flask` app. https://commonmark.org/

.. toctree::
   :maxdepth: 2
   :caption: Contents:

.. automodule:: flask_commonmark
    :members:
    :private-members:
    :undoc-members:
    :show-inheritance:
    :imported-members:


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
